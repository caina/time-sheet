import 'package:flutter_test/flutter_test.dart';
import 'package:time_sheet/scenes/tracker/tracker.provider.dart';

void main() {
  test("TimeTrack returns a duration", () {
    var track = new TimeTrack(DateTime.now().subtract(new Duration(hours: 2)));
    expect(track.duration.inHours, 2);
  });

  test("Should be able to start a timer", () {
    final provider = new TrackerProvider();

    provider.startTimer(DateTime.now().subtract(new Duration(hours: 1)));

    expect(provider.currentTimer(), contains("1"));
  });

  test("I can start a timer without a value", () {
    final provider = new TrackerProvider();
    provider.startTimer();
    expect(provider.currentTimer(), contains("0"));
  });

  test("Should be able to stop a timer", () {
    final provider = new TrackerProvider();
    provider.startTimer(DateTime.now().subtract(new Duration(
      hours: 4,
      minutes: 50,
    )));
    provider.stopTimer(DateTime.now().subtract(new Duration(hours: 1)));
    expect(provider.currentTimer(), contains("3:50"));
  });

  test("Timer has 2 days, should get the total time for today", () {
    final provider = new TrackerProvider();

    provider.startTimer(DateTime.now().subtract(new Duration(
      days: 3,
      hours: 4,
    )));
    provider.stopTimer(DateTime.now().subtract(new Duration(
      days: 3,
      hours: 2,
    )));

    provider.startTimer(DateTime.now().subtract(new Duration(
      days: 2,
      hours: 4,
    )));
    provider.stopTimer(DateTime.now().subtract(new Duration(
      days: 2,
      hours: 2,
    )));

    provider.startTimer(DateTime.now().subtract(new Duration(minutes: 10)));
    provider.stopTimer();
    expect(provider.currentTimer(), contains("0:10"));
  });

  test("Starting and stopting an time in the same day", () {
    final provider = new TrackerProvider();
    provider.startTimer(DateTime.now().subtract(new Duration(hours: 2)));
    provider.stopTimer(DateTime.now().subtract(new Duration(hours: 1)));

    provider.startTimer(DateTime.now().subtract(new Duration(minutes: 10)));
    provider.stopTimer();

    expect(provider.currentTimer(), contains("1:10"));
  });

  test("getting timer from empty list should no break", () {
    final provider = new TrackerProvider();
    expect(provider.currentTimer(), contains("0:00"));
  });

  test("Tracker running should return true", () {
    final provider = new TrackerProvider();
    provider.startTimer();
    expect(provider.isRunning, true);
  });

  test("Tracker running should return true", () {
    final provider = new TrackerProvider();
    provider.startTimer();
    provider.stopTimer();
    expect(provider.isRunning, false);
  });

  test("Toggling timer to start", () {
    final provider = new TrackerProvider();
    provider.toggleTimer();
    expect(provider.isRunning, true);
  });

  test("Toggling timer to stop", () {
    final provider = new TrackerProvider();
    provider.toggleTimer();
    provider.toggleTimer();
    expect(provider.isRunning, false);
  });

  test("Toggling multiple times to start", () {
    final provider = new TrackerProvider();
    provider.toggleTimer();
    provider.toggleTimer();
    provider.toggleTimer();
    provider.toggleTimer();
    provider.toggleTimer();
    expect(provider.isRunning, true);
  });

  test("Should give the time to leave to job", () {
    final provider = new TrackerProvider();
    provider.startTimer(DateTime.now().subtract(Duration(hours: 7)));
    provider.stopTimer();

    expect(provider.timeToLeave(), contains("1"));
  });

  test("Should give the time to leave to job as 2 hours", () {
    final provider = new TrackerProvider();
    provider.startTimer(DateTime.now().subtract(Duration(hours: 6)));
    provider.stopTimer();

    expect(provider.timeToLeave(), contains("2"));
  });

  test("Should not calculate time for same day from different months", () {
    final provider = new TrackerProvider();
    provider.startTimer(new DateTime(2019, 10, 8));
    provider.stopTimer(new DateTime(2019, 10, 8, 1));

    expect(provider.currentTimer(), contains("0:00:00"));
  });

  test("Should calculate time for week", () {
    final provider = new TrackerProvider();
    provider.startTimer(new DateTime(2019, 11, 4));
    provider.stopTimer(new DateTime(2019, 11, 8, 1));

    expect(provider.calculateWeeklyTime(new DateTime(2019, 11, 8, 2)),
        contains("97"));
  });

  test("Should calculate time for week from monday to sunday", () {
    final provider = new TrackerProvider();
    provider.startTimer(new DateTime(2019, 11, 4, 8));
    provider.stopTimer(new DateTime(2019, 11, 4, 16));
    provider.startTimer(new DateTime(2019, 11, 5, 8));
    provider.stopTimer(new DateTime(2019, 11, 5, 16));
    provider.startTimer(new DateTime(2019, 11, 6, 8));
    provider.stopTimer(new DateTime(2019, 11, 6, 16));
    provider.startTimer(new DateTime(2019, 11, 7, 8));
    provider.stopTimer(new DateTime(2019, 11, 7, 16));
    provider.startTimer(new DateTime(2019, 11, 8, 8));
    provider.stopTimer(new DateTime(2019, 11, 8, 16));
    provider.startTimer(new DateTime(2019, 11, 9, 8));
    provider.stopTimer(new DateTime(2019, 11, 9, 16));
    provider.startTimer(new DateTime(2019, 11, 10, 8));
    provider.stopTimer(new DateTime(2019, 11, 10, 16));

    expect(provider.calculateWeeklyTime(new DateTime(2019, 11, 10, 17)),
        contains("56"));
  });

  test("Should calculate time for week from monday to sunday", () {
    final provider = new TrackerProvider();
    final testDay = new DateTime(2019, 11, 2, 17);
    provider.startTimer(new DateTime(2019, 10, 28, 8));
    provider.stopTimer(new DateTime(2019, 10, 28, 16));
    provider.startTimer(new DateTime(2019, 10, 29, 8));
    provider.stopTimer(new DateTime(2019, 10, 29, 16));
    provider.startTimer(new DateTime(2019, 10, 30, 8));
    provider.stopTimer(new DateTime(2019, 10, 30, 16));
    provider.startTimer(new DateTime(2019, 10, 31, 8));
    provider.stopTimer(new DateTime(2019, 10, 31, 16));
    provider.startTimer(new DateTime(2019, 11, 1, 8));
    provider.stopTimer(new DateTime(2019, 11, 1, 16));

    expect(provider.calculateWeeklyTime(testDay), contains("40"));
  });

  test("Should calculate time for no working", () {
    final provider = new TrackerProvider();
    expect(provider.calculateWeeklyTime(DateTime.now()), contains("0:00:00"));
  });

  test("Should calculate time for monthly working", () {
    final provider = new TrackerProvider();
    expect(provider.monthlyTimer(), contains("0:00:00"));
  });

  test("Should calculate time for monthly working", () {
    final provider = new TrackerProvider();
    provider.startTimer(DateTime.now());
    provider.stopTimer(DateTime.now().add(Duration(hours: 5)));

    expect(provider.monthlyTimer(), contains("5"));
  });

  test("Should calculate time for monthly working at the start of the month",
      () {
    final provider = new TrackerProvider();
    final nowDate = DateTime.now();
    final startOfMonth = new DateTime(nowDate.year, nowDate.month, 1);
    provider.startTimer(startOfMonth);
    provider.stopTimer(startOfMonth.add(Duration(hours: 5)));

    expect(provider.monthlyTimer(), contains("5"));
  });

  test("Should calculate time for monthly working at the end of the month", () {
    final provider = new TrackerProvider();
    final nowDate = DateTime.now();
    final endOfMonth = new DateTime(nowDate.year, nowDate.month + 1, 0);
    provider.startTimer(endOfMonth);
    provider.stopTimer(endOfMonth.add(Duration(hours: 5)));

    expect(provider.monthlyTimer(), contains("5"));
  });
/*
  test("Should calculate time for monthly working", () {
    final provider = new TrackerProvider();
    provider.startTimer(DateTime.now());
    provider.stopTimer(DateTime.now().add(Duration(hours: 5)));

    expect(provider.monthlyTimer(), contains("5"));
  });

  test("Should calculate time for monthly working", () {
    final provider = new TrackerProvider();
    provider.startTimer(DateTime.now());
    provider.stopTimer(DateTime.now().add(Duration(hours: 5)));

    expect(provider.monthlyTimer(), contains("5"));
  });*/
}
