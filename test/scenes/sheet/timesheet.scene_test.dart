import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:time_sheet/scenes/sheet/timesheet.provider.dart';
import 'package:time_sheet/scenes/sheet/timesheet.scene.dart';

void main() {
  Widget createScene() {
    return MaterialApp(
        home: ChangeNotifierProvider<TimeSheetProvider>.value(
      value: TimeSheetProvider(),
      child: TimeSheetScene(),
    ));
  }

  testWidgets("My widget has a title", (WidgetTester tester) async {
    await tester.pumpWidget(createScene());

    final titleFinder = find.text('Time range calculation');

    expect(titleFinder, findsOneWidget);
  });

  testWidgets("I have an add button", (WidgetTester tester) async {
    await tester.pumpWidget(createScene());
    expect(find.byKey(Key("timer-add-button")), findsOneWidget);
  });

//  testWidgets("by default, I have 2 timers", (WidgetTester tester) async {
//    await tester.pumpWidget(createScene());
//
//    expect(find.byElementType(TimeCard), findsNWidgets(2));
//  });
}
