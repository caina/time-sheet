import 'package:flutter_test/flutter_test.dart';
import 'package:time_sheet/scenes/sheet/timesheet.provider.dart';

void main() {
  test("time sheet provider starts with 2 elements", () {
    final provider = TimeSheetProvider();
    expect(provider.timers.length, 2);
  });

  test("must be able to add an entry", (){
    final provider = TimeSheetProvider();
    provider.createTimer();
    expect(provider.timers.length, 3);
  });

  test("must be able to sum timmers", (){
    final provider = TimeSheetProvider();

    for (var timer in provider.timers) {
      timer.startTimerChanger("00:00");
      timer.endTimerChange("01:00");
    }

    provider.parseTotal();

    expect(provider.totalTime, "2:00");
  });

  test("must be able to sum timmers when a new element is introduced", (){
    final provider = TimeSheetProvider();
    provider.createTimer();

    for (var timer in provider.timers) {
      timer.startTimerChanger("00:00");
      timer.endTimerChange("01:00");
    }

    provider.parseTotal();

    expect(provider.totalTime, "3:00");
  });
}
