import 'package:flutter_test/flutter_test.dart';
import 'package:time_sheet/scenes/sheet/time.provider.dart';

void main() {
  group("diff calculation", (){
    test('should give a difference of an hour', (){
      final timeProvider = TimeProvider();

      timeProvider.startTimerChanger("00:00");
      timeProvider.endTimerChange("01:00");

      final Duration duration = timeProvider.calculateTimeDiff();
      expect(duration.inHours, 1);
    });

    test("should calculate a diff of a day", () {
      final timeProvider = TimeProvider();

      timeProvider.startTimerChanger("08:00");
      timeProvider.endTimerChange("16:00");

      final Duration duration = timeProvider.calculateTimeDiff();
      expect(duration.inHours, 8);
    });

    test("should be able to calculate minutes",  () {
      final timeProvider = TimeProvider();

      timeProvider.startTimerChanger("00:10");
      timeProvider.endTimerChange("00:20");

      final Duration duration = timeProvider.calculateTimeDiff();
      expect(duration.inMinutes, 10);
    });

    test("should be able to calculate hours with minutes",  () {
      final timeProvider = TimeProvider();

      timeProvider.startTimerChanger("00:10");
      timeProvider.endTimerChange("10:40");

      final Duration duration = timeProvider.calculateTimeDiff();
      expect(duration.inMinutes, 630);
    });
  });

  group("asString", (){
    test("covert a full diff to string",  () {
      final timeProvider = TimeProvider();

      timeProvider.startTimerChanger("00:12");
      timeProvider.endTimerChange("10:40");

      String time = TimeProvider.asString(timeProvider.calculateTimeDiff());
      expect(time, "10:28");
    });

    test("must convert as one hour", (){
      final timeProvider = TimeProvider();

      timeProvider.startTimerChanger("00:01");
      timeProvider.endTimerChange("01:00");

      String time = TimeProvider.asString(timeProvider.calculateTimeDiff());
      expect(time, "0:59");
    });
  });
}