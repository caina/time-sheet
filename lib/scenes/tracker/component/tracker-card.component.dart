import 'package:flutter/material.dart';

class TrackerCard extends StatelessWidget {
  final String title;
  final String text;
  final String subtext;

  const TrackerCard({Key key, this.title, this.text, this.subtext});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/sunset.jpg"),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          padding: EdgeInsets.all(20),
          width: double.infinity,
          height: 200,
          child: Column(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Container(
                  child: Text(this.title),
                ),
              ),
              Flexible(
                flex: 4,
                child: Container(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      this.text,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 50),
                    ),
                    Text(this.subtext)
                  ],
                )),
              ),
            ],
          )),
    );
  }
}
