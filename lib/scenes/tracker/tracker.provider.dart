import 'package:flutter/cupertino.dart';

class TimeTrack {
  DateTime startTime;
  DateTime endTime;

  TimeTrack(this.startTime);

  Duration get duration {
    var end = this.endTime != null ? this.endTime : DateTime.now();
    var inMilliseconds = end.difference(this.startTime).inMilliseconds;
    return new Duration(milliseconds: inMilliseconds);
  }
}

class TrackerProvider with ChangeNotifier {
  final List<TimeTrack> trackers = [];

  void startTimer([DateTime date]) {
    if (date == null) {
      date = DateTime.now();
    }

    var timeTrack = new TimeTrack(date);
    this.trackers.add(timeTrack);
    notifyListeners();
  }

  void stopTimer([DateTime date]) {
    final lastTime = trackers.last;
    lastTime.endTime = date != null ? date : DateTime.now();
    notifyListeners();
  }

  void toggleTimer() {
    if (trackers.length == 0) {
      this.startTimer();
      return;
    }

    if (trackers.last.endTime == null) {
      this.stopTimer();
    } else {
      this.startTimer();
    }
  }

  bool get isRunning {
    if (trackers.length == 0) {
      return false;
    }

    return trackers.last.endTime == null;
  }

  String timeToLeave() {
    return formatDuration(Duration(hours: 8) - totalDuration);
  }

  Duration get totalDuration {
    if (trackers.length == 0) {
      return Duration();
    }

    final currentDay = DateTime.now().day;
    final currentMonth = DateTime.now().month;
    final currentYear = DateTime.now().year;

    return trackers
        .where((tracker) => tracker.startTime.day == currentDay)
        .where((tracker) => tracker.startTime.month == currentMonth)
        .where((tracker) => tracker.startTime.year == currentYear)
        .map((tracker) => tracker.duration)
        .fold(new Duration(), (prev, next) => prev + next);
  }

  String currentTimer() {
    return formatDuration(totalDuration);
  }

  String formatDuration(Duration duration) {
    final diff = duration.toString().split(":");
    return "${diff[0]}:${diff[1]}:${diff[2].split(".")[0]}";
  }

  String calculateWeeklyTime(DateTime retrievalDate) {
    final startOfWeekOffset = Duration(
        days: retrievalDate.weekday - 1,
        hours: retrievalDate.hour,
        minutes: retrievalDate.minute,
        seconds: retrievalDate.second,
        milliseconds: retrievalDate.millisecond,
        microseconds: retrievalDate.microsecond);
    final startOfWeekDate = retrievalDate.subtract(startOfWeekOffset);

    final Duration weeklyDuration = trackers
        .where((tracker) =>
            tracker.startTime.isAfter(startOfWeekDate) ||
            tracker.startTime == startOfWeekDate)
        .where((tracker) =>
            tracker.startTime.isBefore(startOfWeekDate.add(Duration(days: 7))))
        .map((tracker) => tracker.duration)
        .fold(new Duration(), (prev, next) => prev + next);
    return formatDuration(weeklyDuration);
  }

  Duration get totalMonthlyDuration {
    if (trackers.length == 0) {
      return Duration();
    }
    final currentMonth = DateTime.now().month;
    final currentYear = DateTime.now().year;

    return trackers
        .where((tracker) => tracker.startTime.month == currentMonth)
        .where((tracker) => tracker.startTime.year == currentYear)
        .map((tracker) => tracker.duration)
        .fold(new Duration(), (prev, next) => prev + next);
  }

  String monthlyTimer() {
    return formatDuration(totalMonthlyDuration);
  }
}
