import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:time_sheet/scenes/tracker/tracker.provider.dart';

import 'component/tracker-card.component.dart';

class TimeTrackerScene extends StatefulWidget {
  static const routeUrl = "/time-tracker";

  @override
  _TimeTrackerSceneState createState() => _TimeTrackerSceneState();
}

class _TimeTrackerSceneState extends State<TimeTrackerScene> {
  @override
  void initState() {
    super.initState();

    Timer.periodic(Duration(seconds: 1), (Timer t) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var trackerProvider = Provider.of<TrackerProvider>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text("Time tracker"),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          key: ValueKey("timer-add-button"),
          onPressed: trackerProvider.toggleTimer,
          backgroundColor:
              trackerProvider.isRunning ? Colors.red : Colors.green,
          child: trackerProvider.isRunning
              ? Icon(Icons.stop)
              : Icon(Icons.play_arrow),
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Theme.of(context).primaryColor,
          currentIndex: 1,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.playlist_add_check, color: Colors.amberAccent),
                title: new Text('spread')),
            BottomNavigationBarItem(
                icon: Icon(Icons.shutter_speed,
                    color: Theme.of(context).secondaryHeaderColor),
                title: new Text('calculate')),
          ],
          onTap: (index) {},
        ),
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Flexible(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).secondaryHeaderColor),
                  child: Center(
                    child: Text(
                      trackerProvider.currentTimer(),
                      style: Theme.of(context).textTheme.display1,
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 3,
                child: Container(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        TrackerCard(
                          title: "Leave by",
                          text: "17:20",
                          subtext: "Total this week: 23h",
                        ),
                        TrackerCard(
                          title: "Total time this week",
                          text: "45.6h",
                          subtext: "Total this month: 99h",
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
